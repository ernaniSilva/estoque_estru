<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>lista módulo</title>
	<!-- HTML 4 -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<!-- HTML5 -->
	<meta charset="utf-8"/>
	
	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

    <?php
        $conex = mysqli_connect('cabot1.mysql.dbaas.com.br','cabot1','c@b0t!1311','cabot1');
        
        if(!empty($_GET['id'])){

            $sql = "DELETE FROM modulo WHERE id =".$_GET['id'];
            $result = mysqli_query($conex, $sql);
            $del = 'ok';
        }

        $sql = 'SELECT * FROM modulo';

        $result = mysqli_query($conex, $sql);    
    ?>
<style>
    body {
       padding-top: 1em;
    }
</style>
</head>

<body>
    <?php if(!empty($del)) {?>
        <div class="container-fluid">		
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Parabéns!</strong> Atividade Excluida com sucesso!
            </div>    
        </div>
    <?php } ?>
	<div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Título</th>
                    <th scope="col">Descrição</th>
                    <th scope="col">Status</th>
                    <th scope="col">data de atualização</th>
                    <th colspan="2">Botões</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($result as $mod) { ?>
                    <tr>
                    <th scope="row"><?php echo($mod['id'])?></th>
                    <td scope="row"><?php echo($mod['titulo'])?></td>
                    <td scope="row"><?php echo($mod['descricao'])?></td>
                    <td scope="row"><?php echo($mod['status'] == 1 ? 'Ativo' : 'inativo')?></td>
                    <td scope="row">
                        <?php
                            $data_mysql = $mod['dt_update'];
                            $timestamp = strtotime($data_mysql);
                            echo date('d/m/Y H:i:s', $timestamp); 
                        ?>
                    </td>
                    <td scope="row"><a href='editar.php?id=<?php echo($mod['id'])?>'>Editar</a></td>
                    <td scope="row"><a href='lista_modulo.php?id=<?php echo($ativ['id'])?>'>Deletar</a></td>
                    </tr>
                <?php } ?>    
            </tbody>
        </table>
	</div>	
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

    $(function () {
        $('[data-toggle="popover"]').popover()
    })
</script>
<?php mysqli_close($conex); ?>

</html>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Edição Módulo</title>
<!-- HTML 4 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- HTML5 -->
<meta charset="utf-8"/>

<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

<?php
    $conex = mysqli_connect('cabot1.mysql.dbaas.com.br','cabot1','c@b0t!1311','cabot1');

    $id = $_GET[id];

    $sql = 'SELECT * FROM atividade WHERE id='.$id;
    $result = mysqli_query($conex, $sql);
    $dados = mysqli_fetch_object($result);

    $sql_mod = 'SELECT * FROM modulo';
    $result_mod = mysqli_query($conex, $sql_mod);
?>
</head>

<body>
<section class="contato">
    <div class="container">
        <div class="col-sm-8 col-md-5">
          <h4>Editar Atividade</h4>
            <form id="contact" method="post" action="update_atv.php" enctype="multipart/form-data">
                <div class="form-group">
                    <input class="form-control txt-primary" value="<?=$dados->id?>" name="id" type="hidden">
                    <input class="form-control txt-primary" value="<?=date('Y-m-d H:i:s')?>" name="dateupdate" type="hidden">
                </div>
                <div class="form-group">
                    <input class="form-control txt-primary" value="<?=$dados->titulo?>" name="titulo" type="text" required>
                </div>
                <div class="form-group">
                    <input class="form-control txt-primary" value="<?=$dados->descricao?>" name="descricao" type="text" required>
                </div>
                
                <div class="form-group">
                    <select class="dd-primary" name="modulo" title="Status">
                        <?php foreach($result_mod as $mod) { ?>
                            <option value="<?=$mod['id']?>" <?=$dados->id_modulo == $mod['id'] ? 'selected' : '' ?>><?=$mod['titulo']?></option>
                        <?php } ?>    
                    </select>
                </div>                    
                <div class="form-group">
                    <select class="dd-primary" name="status" title="Status">
                        <option value="1" <?=$dados->status == 1 ? 'selected' : '' ?>>Ativo</option>
                        <option value="0" <?=$dados->status == 0 ? 'selected' : '' ?>>Inativo</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-secundary">Enviar</button>
          </form>
        </div>
    </div>
</section>
</body>
</html>

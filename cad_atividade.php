<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Nova Atividade</title>
<!-- HTML 4 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- HTML5 -->
<meta charset="utf-8"/>

<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

<?php
    $conex = mysqli_connect('cabot1.mysql.dbaas.com.br','cabot1','c@b0t!1311','cabot1');

    $sql_mod = 'SELECT * FROM modulo';
    $result_mod = mysqli_query($conex, $sql_mod);
?>
</head>

<body>
<section>
    <div class="container">
        <div class="col-sm-8 col-md-5">
          <h4>Nova Atividade</h4>
            <form method="post" action="create_atv.php" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group">
                        <input class="form-control txt-primary" value="" name="id" type="hidden">
                    </div>
                    <div class="form-group col-md-6">
                        <input class="form-control txt-primary" value="" name="titulo" type="text" required>
                    </div>
                    <div class="form-group col-md-6">
                        <input class="form-control txt-primary" value="" name="descricao" type="text" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-3">
                        <select class="custom-select" name="modulo" title="Status">
                            <?php foreach($result_mod as $mod) { ?>
                                <option value="<?=$mod['id']?>" <?=$dados->id_modulo == $mod['id'] ? 'selected' : '' ?>><?=$mod['titulo']?></option>
                            <?php } ?>    
                        </select>
                    </div>                    
                    <div class="form-group col-md-3">
                        <select class="custom-select" name="status" title="Status">
                            <option value="1">Ativo</option>
                            <option value="0">Inativo</option>
                        </select>
                    </div>
                </div>    
                <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </div>
    </div>
</section>
</body>
</html>
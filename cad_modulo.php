<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Novo Módulo</title>
<!-- HTML 4 -->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!-- HTML5 -->
<meta charset="utf-8"/>

<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>
<section>
    <div class="container">
        <div class="col-sm-8 col-md-5">
          <h4>Novo Módulo</h4>
            <form id="contact" method="post" action="create_mod.php" enctype="multipart/form-data">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <input class="form-control txt-primary" value="" name="titulo" type="text" required>
                    </div>
                    <div class="form-group col-md-6">
                        <input class="form-control txt-primary" value="" name="descricao" type="text" required>
                    </div>
                </div>    
                <div class="form-group">
                    <select class="custom-select" name="status" title="Status">
                        <option value="1">Ativo</option>
                        <option value="0">Inativo</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">Enviar</button>
          </form>
        </div>
    </div>
</section>
</body>
</html>
